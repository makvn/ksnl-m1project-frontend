/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import Header from 'components/Header';
import Footer from 'components/Footer';
import messages from './messages';

export default function HomePage() {
  return (
    <div className="bg-top-page">
      <Header />
      <h1>
        <FormattedMessage {...messages.header} />
      </h1>
      <Footer />
    </div>
  );
}

import IcoDashboard from '@material-ui/icons/Dashboard';
import DashboardPage from 'containers/Admin/Dashboard/Loadable';

const adminRoutes = [
  {
    path: '/dashboard',
    name: 'Dashboard',
    icon: IcoDashboard,
    component: DashboardPage,
    layout: '/admin',
    authenticated: true,
  },
];

export default adminRoutes;

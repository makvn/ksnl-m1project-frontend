/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Sidebar from 'components/Admin/Sidebar';
import Navbar from 'components/Admin/Navbars';
import withStyles from '@material-ui/core/styles/withStyles';
import 'styles/material-dashboard-react.css?v=1.7.0';
import logo from 'images/logo.png';
import image from 'images/sidebar-2.jpg';
import routes from './routes.js';
import dashboardStyle from "styles/material-dashboard-react/layouts/dashboardStyle.js";

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.layout === '/admin') {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      return null;
    })}
    {/* <Redirect from="/admin" to="/admin/dashboard" /> */}
  </Switch>
);

class AdminPage extends React.Component {
  state = {
    image,
    color: 'blue',
    hasImage: true,
    fixedClasses: 'dropdown show',
    mobileOpen: false,
  };

  mainPanel = React.createRef();

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  render() {
    const { classes, ...rest } = this.props;
    console.log('classes', classes);
    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={routes}
          logoText=""
          logo={logo}
          image={this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          {...rest}
        />
        <div className={classes.mainPanel} ref={this.mainPanel}>
          <Navbar
            routes={routes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
          <div className={classes.content}>
            <div className={classes.container}>{switchRoutes}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(dashboardStyle)(AdminPage);

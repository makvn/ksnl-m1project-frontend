import React from 'react';
import Logo from 'images/logo.png';
import IcoSearch from 'images/ic-search.png';

function Header() {
  return (
    <nav className="navbar-ksnl">
      <div className="container">
        <div className="row">
          <div className="navbar-header">
            <a className="logo-ksnl" href="index.html">
              <img src={Logo} alt="logo" />
            </a>
            <button type="button" className="navbar-toggle collapsed" data-toggle="flyout"
                    data-target=".navbar-flyout" aria-expanded="false">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>
          <div className="navbar-flyout flyout">
            <ul className="nav-ksnl">
              <li>
                <a className="active" href="index.html">
                  ksnl sport club{' '}
                </a>
              </li>
              <li>
                <a href="about-us.html">Blog & News</a>
              </li>
              <li>
                <a href="practice-areas.html">Class Schedule</a>
              </li>
              <li>
                <a href="#">Owner Zone</a>
              </li>
              <li>
              <span className="icon-search">
                <img src={IcoSearch} alt="Search" />
              </span>
              </li>
              <li>
                <a
                  className="bt-login"
                  href="#"
                  data-toggle="modal"
                  data-target=".bs-login"
                >
                  Login
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Header;

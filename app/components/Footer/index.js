import React from 'react';
import LogoFooter from 'images/logo1.png';

function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-md-3">
            <div>
              <img src={LogoFooter} />
            </div>
          </div>
          <div className="col-md-4">
            <div className="text-address">
              P: 0832 270 405
              <br />
              M: leyennhi274@gmail.com
              <br />
              A: 20 Nguyen Dang Giai, Thao Dien, D2, Vietnam
            </div>
          </div>
          <div className="col-md-3">
            <div className="nav-content">
              <p>Quick links</p>
              <ul>
                <li>
                  <a href="#">Home</a>
                </li>
                <li>
                  <a href="#">Contact</a>
                </li>
                <li>
                  <a href="#">Class Schedule</a>
                </li>
                <li>
                  <a href="#">Administration</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-md-2">
            <div className="nav-content">
              <p>Documentation</p>
              <ul>
                <li>
                  <a href="#">Blog & News</a>
                </li>
                <li>
                  <a href="#">Support</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-md-12">
            <div className="box-copyright">
              Copyright 2019 | KSNL Team @KSNL
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

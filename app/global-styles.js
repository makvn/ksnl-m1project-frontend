import { createGlobalStyle } from 'styled-components';
import Graphic from 'images/graphic.png';
import Graphic2 from 'images/graphic2.png';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  
  .bg-top-page { background: url(${Graphic}) no-repeat top center; width: 100%; }

  .bg-left-page { background: url(${Graphic2}) no-repeat left center; width: 100%; }
  
  .navbar-ksnl { width: 100%; }
  .navbar-ksnl .logo-ksnl { padding: 10px 0; float: left; width: 300px; }
  .navbar-ksnl .logo-ksnl img { max-width: 100%; height: auto; }
  .navbar-ksnl .nav-ksnl { margin: 0; padding: 25px 0; list-style-type: none; float: right; }
  .navbar-ksnl .nav-ksnl li { float: left; display: block; list-style-type: none; margin: 0; padding: 0; position: relative; font-family: "AndaleMono"; font-weight: 200; }
  .navbar-ksnl .nav-ksnl li a { color: #d2d2d2; font-size: 14px; padding: 0px 15px; display: block; }
  .navbar-ksnl .nav-ksnl li a:hover, .navbar-ksnl .nav-ksnl li a:focus { color: #ffffff; background: #0f0f0f; text-decoration: none; }
  .navbar-ksnl .nav-ksnl li a i { display: none; }
  .navbar-ksnl .nav-ksnl li .active { color: #ffffff !important; background: #0f0f0f; text-transform: uppercase; }
  .navbar-ksnl .nav-ksnl li .bt-login { background: #c12e2a; border-radius: 50px; padding: 4px 20px; color: #fff; text-transform: uppercase; margin-left: 10px; }
  .navbar-ksnl .nav-ksnl li .bt-login:hover { background: #c12e2a; text-decoration: navy; }
  .navbar-ksnl .nav-ksnl li .icon-search { padding-left: 30px; }
  .navbar-ksnl .nav-ksnl li .icon-search img { width: 24px; height: 24px; }
  .navbar-ksnl .nav-ksnl li .icon-email { padding-left: 15px; }
  .navbar-ksnl .nav-ksnl li .icon-email img { width: 24px; height: 24px; }
  
  .top-banner { min-height: 400px; font-size: 32px; color: #fff; padding: 5px 0 0 0; margin-top: 55px; width: 30%; line-height: 35px; position: relative; }
  .top-banner:after { width: 25px; height: 4px; background: #e21838; position: absolute; top: 0; left: 0; content: ''; }
  .top-banner span { display: block; font-size: 14px; padding-top: 10px; }
  .top-banner .bt-register { border: none; background: #e21838; padding: 7px 15px; text-transform: uppercase; color: #fff; text-align: center; outline: none; font-size: 12px; letter-spacing: 4px; margin: 50px auto 20px; width: 240px; position: relative; cursor: pointer; }
  .top-banner .bt-register:after { width: 240px; height: 44px; border: 1px solid #e21838; position: absolute; top: 10px; left: 5px; content: ''; }
  
  .box-new { padding: 40px; text-align: center; font-size: 24px; color: #000000; }
  .box-new span { display: block; background: #333333; color: #fff; width: 70px; padding: 3px 5px; text-transform: uppercase; margin: auto; font-size: 12px; letter-spacing: 3.1px; }
  
  .carousel-inner { padding-bottom: 20px; }
  
  .icon-control-left { position: absolute; width: 42px; height: 42px; top: 35%; left: -15px; }
  
  .icon-control-right { position: absolute; width: 42px; height: 42px; top: 35%; right: -15px; }
  
  .box-slide { margin: 10px 0 30px; padding: 0; list-style-type: none; display: flex; }
  .box-slide li { width: 33.3%; margin: 0; padding: 0 10px; list-style-type: none; }
  .box-slide li .item-slice { width: 100%; position: relative; padding: 15px 20px; border: 1px solid #8c8c8c; font-size: 14px; color: #333333; height: 150px; background: #ffffff; }
  .box-slide li .item-slice:after { position: absolute; border: 1px solid #8c8c8c; width: 100%; height: 150px; top: 10px; left: 10px; content: ''; z-index: -2; }
  
  .box-pic { padding: 30px 0; }
  .box-pic img { max-width: 100%; height: auto; }
  
  .text-become { font-size: 32px; color: #000; padding: 5px 0 20px 0; margin-top: 55px; line-height: 35px; position: relative; }
  .text-become:after { width: 25px; height: 4px; background: #e21838; position: absolute; top: 0; left: 0; content: ''; }
  
  .box-text-become { padding: 0 100px 0 0; margin-left: 30px; border-left: 1px dotted #42434b; }
  .box-text-become p { display: flex; padding: 0; margin: 25px 0 20px -20px; font-size: 16px; }
  .box-text-become p img { width: 40px; height: 40px; margin-right: 10px; }
  
  footer { border-top: 1px solid #dbdbdb; color: #000; padding: 30px 0 60px 0; }
  footer .text-address { padding-top: 100px; font-size: 18px; color: #000; line-height: 30px; }
  footer .nav-content { color: #000; font-size: 18px; padding-top: 60px; }
  footer .nav-content p { margin: 0; padding: 0 0 10px 0; font-size: 20px; position: relative; }
  footer .nav-content p:after { width: 20px; height: 3px; background: #e21838; position: absolute; top: -5px; left: 0; content: ''; }
  footer .nav-content ul { padding: 0; }
  footer .nav-content ul li { margin: 0; padding: 5px 0; display: block; list-style-type: none; }
  footer .nav-content ul li a { color: #333333; font-weight: normal; text-decoration: none; display: block; }
  footer .nav-content ul li a:hover { color: #333333; text-decoration: none; }
  footer .nav-content ul li a[target=_blank] { color: #ec9600; }
  footer .box-copyright { background: #f2f5fa; margin-top: 40px; padding: 20px; text-align: center; border-top: 1px solid #dbdbdb; }
  
  .bg-register { width: 100%; background: #fff url("../images/graphic2.png") no-repeat -400px 0; display: flex; position: relative; }
  .bg-register .pic-l { padding-top: 90%; }
  .bg-register .pic-l img { max-width: 100%; height: auto; }
  .bg-register .register-content { font-family: "Roboto"; width: 100%; padding: 50px 0; }
  .bg-register .register-content .title-rg { margin: 0; padding: 0 0 10px 0; position: relative; font-size: 32px; font-weight: 600; }
  .bg-register .register-content .title-rg:after { width: 20px; height: 3px; background: #e21838; position: absolute; top: -5px; left: 0; content: ''; }
  .bg-register .register-content .note-login a { color: #c12e2a; text-decoration: none; margin-left: 30px; }
  .bg-register .register-content .note-login a:hover { text-decoration: none; }
  .bg-register .register-content .box-form { display: flex; flex-wrap: wrap; padding: 30px 0; margin-left: -10px; margin-right: -10px; }
  .bg-register .register-content .box-form .tt-t { width: 100%; border-bottom: 2px solid #8c8c8c; margin: 0 10px 10px 10px; }
  .bg-register .register-content .box-form .tt-t span { background: #0f0f0f; color: #fff; font-size: 12px; padding: 4px 20px; text-transform: uppercase; letter-spacing: 3.1px; width: 180px; text-align: center; display: block; }
  .bg-register .register-content .box-form .box-txt-1 { width: 50%; padding: 10px; }
  .bg-register .register-content .box-form .box-txt-1 label { display: block; text-transform: uppercase; padding-bottom: 5px; width: 100%; }
  .bg-register .register-content .box-form .box-txt-1 input { width: 100%; height: 38px; border: none; background: #e8e9ef; margin-bottom: 10px; outline: none; color: #000; padding: 5px; }
  .bg-register .register-content .box-form .box-txt-1 input:focus { border: 1px solid #e21838; background: #fff; }
  .bg-register .register-content .box-form .box-txt-2 { width: 100%; padding: 10px; }
  .bg-register .register-content .box-form .box-txt-2 label { display: block; text-transform: uppercase; padding-bottom: 5px; width: 100%; }
  .bg-register .register-content .box-form .box-txt-2 input { width: 100%; height: 38px; border: none; background: #e8e9ef; margin-bottom: 10px; outline: none; color: #000; padding: 5px; }
  .bg-register .register-content .box-form .box-txt-2 input:focus { border: 1px solid #e21838; background: #fff; }
  .bg-register .register-content .box-form .box-check { padding: 10px; width: 100%; }
  .bg-register .register-content .box-form .bt-continue { background: #42434b; width: 140px; height: 44px; border: none; outline: none; font-size: 12px; letter-spacing: 3.1px; color: white; padding: 5px 10px; position: relative; text-transform: uppercase; margin: 20px 10px; }
  .bg-register .register-content .box-form .bt-continue:after { width: 140px; height: 44px; border: 1px solid #42434b; position: absolute; top: 5px; left: 5px; z-index: 0; content: ''; }
  .bg-register .icon-close { width: 24px; height: 24px; position: absolute; top: 5px; right: 5px; cursor: pointer; z-index: 10; }
  
  .box-login { padding-top: 160px; padding-bottom: 160px; }
`;

export default GlobalStyle;
